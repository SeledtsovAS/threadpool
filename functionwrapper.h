#ifndef FUNCTIONWRAPPER_H
#define FUNCTIONWRAPPER_H

#include <utility>
#include <memory>

class FunctionWrapper {
    struct BaseImpl {
        virtual void call() = 0;
        virtual ~BaseImpl() { }
    };

    template <typename F>
    struct TypeImpl: BaseImpl {
        F m_f;

        TypeImpl(F&& f):
            m_f(std::move(f)) { }

        void call()
        {
            m_f();
        }
    };
public:
    template <typename Function>
    FunctionWrapper(Function&& f):
        m_impl { new TypeImpl<Function> { std::move(f) } }
    { }

    FunctionWrapper() = default;

    FunctionWrapper(const FunctionWrapper&) = delete;
    FunctionWrapper& operator = (const FunctionWrapper&) = delete;

    FunctionWrapper(FunctionWrapper&& wrapper):
        m_impl { std::move(wrapper.m_impl) } { }

    FunctionWrapper& operator = (FunctionWrapper&& wrapper)
    {
        m_impl = std::move(wrapper.m_impl);
        return *this;
    }

    void operator () ()
    {
        m_impl->call();
    }

private:
    std::unique_ptr<BaseImpl> m_impl;
};

#endif // FUNCTIONWRAPPER_H
