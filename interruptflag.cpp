#include <condition_variable>

#include "interruptflag.h"

InterruptFlag::InterruptFlag():
    m_flag { false } { }

void InterruptFlag::set()
{
    m_flag.store(true, std::memory_order_relaxed);
    std::lock_guard<std::mutex> locker(m_mutex);
    if (m_cv)
        m_cv->notify_all();
}

bool InterruptFlag::isSet() const
{
    return m_flag.load(std::memory_order_relaxed);
}

InterruptFlag* InterruptFlag::getInterruptFlagForThisThread()
{
    static thread_local InterruptFlag flag;
    return &flag;
}

void InterruptFlag::setConditionVariable(std::condition_variable& cv)
{
    std::lock_guard<std::mutex> locker(m_mutex);
    m_cv = &cv;
}

void InterruptFlag::clearConditionVariable()
{
    std::lock_guard<std::mutex> locker(m_mutex);
    m_cv = nullptr;
}

InterruptFlag::ClearCondVarOnDestruct::~ClearCondVarOnDestruct()
{
    getInterruptFlagForThisThread()->clearConditionVariable();
}
