#ifndef INTERRUPTFLAG_H
#define INTERRUPTFLAG_H

#include <atomic>
#include <mutex>

namespace std {
class condition_variable;
}

class InterruptFlag {
public:
    InterruptFlag();

    void set();
    bool isSet() const;

    void setConditionVariable(std::condition_variable& cv);
    void clearConditionVariable();

    static InterruptFlag* getInterruptFlagForThisThread();

public:
    struct ClearCondVarOnDestruct {
        ~ClearCondVarOnDestruct();
    };

private:
    std::atomic<bool> m_flag;
    std::mutex m_mutex;
    std::condition_variable* m_cv;
};

#endif // INTERRUPTFLAG_H
