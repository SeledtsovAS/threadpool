#include <condition_variable>
#include <mutex>

#include "interruptiblethread.h"

void InterruptibleThread::interrupt()
{
    if (m_flag)
        m_flag->set();
}

void interruptionPoint()
{
    auto flag =
        InterruptFlag::getInterruptFlagForThisThread();
    if (flag->isSet())
        throw InterruptedThread { };
}

void interruptibleWait(std::condition_variable& cv,
                   std::unique_lock<std::mutex>& lk)
{
    interruptionPoint();
    InterruptFlag::getInterruptFlagForThisThread()->setConditionVariable(cv);
    InterruptFlag::ClearCondVarOnDestruct guard;
    interruptionPoint();
    cv.wait_for(lk, std::chrono::milliseconds(1));
    interruptionPoint();
}
