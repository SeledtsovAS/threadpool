#ifndef INTERRUPTIBLETHREAD_H
#define INTERRUPTIBLETHREAD_H

#include <thread>
#include <future>

namespace std {

class condition_variable;
class mutex;

}

#include "interruptflag.h"

struct InterruptedThread
{ };

class InterruptibleThread {
public:
    template <typename FunctionType>
    InterruptibleThread(FunctionType function);

    void interrupt();

    InterruptibleThread(InterruptibleThread&& impl):
        m_thread(std::move(impl.m_thread)),
        m_flag(impl.m_flag) { }

    virtual ~InterruptibleThread() {
        if (m_thread.joinable())
            m_thread.join();
    }

private:
    std::thread m_thread;
    InterruptFlag* m_flag;
};

template <typename FunctionType>
InterruptibleThread::InterruptibleThread(FunctionType function)
{
    std::promise<InterruptFlag*> p;
    m_thread = std::thread([function, &p]() {
        p.set_value(InterruptFlag::getInterruptFlagForThisThread());
        try {
            function();
        }
        catch (...) {

        }
    });

    m_flag = p.get_future().get();
}

void interruptionPoint();

void interruptibleWait(std::condition_variable& cv,
                   std::unique_lock<std::mutex>& lk);

template <typename Predicate>
void interruptibleWait(std::condition_variable &cv,
                       std::unique_lock<std::mutex> &lk,
                       Predicate pred)
{
    interruptionPoint();
    InterruptFlag::getInterruptFlagForThisThread()->setConditionVariable(cv);
    InterruptFlag::ClearCondVarOnDestruct guard;
    while (!InterruptFlag::getInterruptFlagForThisThread() && !pred()) {
        cv.wait_for(lk, std::chrono::milliseconds(1));
    }
}

#endif // INTERRUPTIBLETHREAD_H
