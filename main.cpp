#include <iostream>

#include "threadsafequeue.h"
#include "threadpool.h"
#include "interruptiblethread.h"

std::condition_variable cv;
std::mutex mutex;

int main(int argc, char** argv) try {
    auto thread = InterruptibleThread([&]() {
        std::unique_lock<std::mutex> l(mutex);
        while (true) {
            std::cout << "2" << std::endl;
            cv.wait(l);
            //interruptibleWait(var, lock1);
            std::cout << "1" << std::endl;
        }
    });
    std::this_thread::sleep_for(std::chrono::seconds(1));
//    thread.interrupt();
    //cv.notify_all();
    while (true) { }

    return 0;
}
catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -1;
}
