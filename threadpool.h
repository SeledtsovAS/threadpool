#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <vector>
#include <thread>
#include <atomic>
#include <type_traits>
#include <future>
#include <queue>

#include "threadsjoiner.h"
#include "threadsafequeue.h"
#include "functionwrapper.h"
#include "workstealingqueue.h"

class ThreadPool {
    using TaskType = FunctionWrapper;
public:
    ThreadPool():
        m_done { false },
        m_joiners { m_threads }
    {
        unsigned int countThreads =
            std::thread::hardware_concurrency();

        try {
            for (unsigned int i = 0; i < countThreads; ++i) {
                m_queues.push_back(
                    std::unique_ptr<WorkStealingQueue>(new WorkStealingQueue)
                );

                m_threads.push_back(std::thread(std::bind(
                    &ThreadPool::threadWorker, this, i))
                );
            }
        }
        catch (...) {
            m_done = true;
            throw;
        }
    }

    virtual ~ThreadPool()
    {
        m_done = true;
    }

    void runPendingTask()
    {
        FunctionWrapper task { };

        if (popTaskFromLocalQueue(task) || popTaskFromGlobalQueue(task) ||
                popTaskFromOtherThreadQueue(task)) {
            task();
        }
        else
            std::this_thread::yield();
    }

    template <typename FunctionType>
    std::future<typename std::result_of<FunctionType()>::type>
    submit(FunctionType function)
    {
        using ResultType =
            typename std::result_of<FunctionType()>::type;

        std::packaged_task<ResultType()> task { std::move(function) };
        std::future<ResultType> future { task.get_future() };

        if (m_localWorkQueue)
            m_localWorkQueue->push(std::move(task));
        else
            m_globalWorkQueue.push(std::move(task));

        return future;
    }

private:
    void threadWorker(unsigned int indexQueue)
    {
        m_indexLocalQueue = indexQueue;
        m_localWorkQueue = m_queues[m_indexLocalQueue].get();

        while (!m_done) {
            runPendingTask();
        }
    }

    bool popTaskFromLocalQueue(TaskType& task)
    {
        return m_localWorkQueue && m_localWorkQueue->try_pop(task);
    }

    bool popTaskFromGlobalQueue(TaskType& task)
    {
        return m_globalWorkQueue.try_pop(task);
    }

    bool popTaskFromOtherThreadQueue(TaskType& task)
    {
        for (auto& queue: m_queues)
            if (queue.get() != m_localWorkQueue && queue->try_steal(task))
                return true;

        return false;
    }

private:
    std::atomic<bool> m_done;

    ThreadSafeQueue<FunctionWrapper> m_globalWorkQueue;
    std::vector<std::unique_ptr<WorkStealingQueue>> m_queues;
    static thread_local WorkStealingQueue* m_localWorkQueue;
    static thread_local unsigned int m_indexLocalQueue;

    std::vector<std::thread> m_threads;
    ThreadsJoiner m_joiners;
};

#endif // THREADPOOL_H
