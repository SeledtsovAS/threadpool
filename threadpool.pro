TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pthread
LIBS += -pthread

SOURCES += main.cpp \
    threadsafequeue.cpp \
    threadsjoiner.cpp \
    threadpool.cpp \
    functionwrapper.cpp \
    workstealingqueue.cpp \
    interruptflag.cpp \
    interruptiblethread.cpp

HEADERS += \
    threadsafequeue.h \
    threadsjoiner.h \
    threadpool.h \
    functionwrapper.h \
    workstealingqueue.h \
    interruptflag.h \
    interruptiblethread.h

