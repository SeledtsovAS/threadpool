#ifndef THREADSAFEQUEUE_H
#define THREADSAFEQUEUE_H

#include <condition_variable>
#include <mutex>
#include <memory>

template <typename T>
class ThreadSafeQueue {
    struct Node;
public:
    ThreadSafeQueue():
        m_head { new Node { } },
        m_tail { m_head.get() }
    { }

    void push(T value)
    {
        std::shared_ptr<T> new_data {
            std::make_shared<T>(std::move(value)) };
        std::unique_ptr<Node> p { new Node { } };
        Node* new_tail = p.get();

        std::lock_guard<std::mutex> locker { m_tailMutex };

        m_tail->m_data = std::move(new_data);
        m_tail->m_next = std::move(p);
        m_tail = new_tail;
    }

    std::shared_ptr<T> try_pop()
    {
        auto res = try_pop_head();
        return res ? res->m_data : nullptr;
    }

    bool try_pop(T& value)
    {
        auto res = try_pop_head(value);
        return bool(res);
    }

private:
    Node* get_tail()
    {
        std::lock_guard<std::mutex> locker(m_tailMutex);
        return m_tail;
    }

    std::unique_ptr<Node> pop_head()
    {
        std::unique_ptr<Node> old_head = std::move(m_head);
        m_head = std::move(old_head->m_next);
        return old_head;
    }

    std::unique_ptr<Node> try_pop_head()
    {
        std::lock_guard<std::mutex> locker(m_headMutex);
        if (m_head.get() == get_tail())
            return nullptr;

        return pop_head();
    }

    std::unique_ptr<Node> try_pop_head(T& value)
    {
        std::lock_guard<std::mutex> locker(m_headMutex);
        if (m_head.get() == get_tail())
            return nullptr;

        value = std::move(*m_head->m_data);
        return pop_head();
    }

private:
    struct Node {
        std::shared_ptr<T> m_data;
        std::unique_ptr<Node> m_next;
    };

    std::mutex m_headMutex;
    std::unique_ptr<Node> m_head;
    std::mutex m_tailMutex;
    Node* m_tail;
};

#endif // THREADSAFEQUEUE_H
