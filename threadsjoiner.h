#ifndef THREADSJOINER_H
#define THREADSJOINER_H

#include <thread>
#include <vector>

class ThreadsJoiner {
public:
    explicit ThreadsJoiner(std::vector<std::thread>& threads):
        m_threads(threads) { }

    virtual ~ThreadsJoiner() noexcept
    {
        for (auto& thread: m_threads)
            if (thread.joinable())
                thread.join();
    }

private:
    std::vector<std::thread>& m_threads;
};

#endif // THREADSJOINER_H
