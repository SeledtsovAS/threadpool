#include "workstealingqueue.h"

void WorkStealingQueue::push(DataType value)
{
    std::lock_guard<std::mutex> locker(m_mutex);
    m_workQueue.push_front(std::move(value));
}

bool WorkStealingQueue::try_pop(DataType &result)
{
    std::lock_guard<std::mutex> locker(m_mutex);
    if (m_workQueue.empty())
        return false;

    result = std::move(m_workQueue.front());
    m_workQueue.pop_front();
    return true;
}

bool WorkStealingQueue::try_steal(DataType &result)
{
    std::lock_guard<std::mutex> locker(m_mutex);
    if (m_workQueue.empty())
        return false;

    result = std::move(m_workQueue.back());
    m_workQueue.pop_back();
    return true;
}

bool WorkStealingQueue::empty() const
{
    std::lock_guard<std::mutex> locker(m_mutex);
    if (m_workQueue.empty())
        return false;
    return true;
}
