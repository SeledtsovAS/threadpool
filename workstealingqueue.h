#ifndef WORKSTEALINGQUEUE_H
#define WORKSTEALINGQUEUE_H

#include <deque>
#include <mutex>

#include "functionwrapper.h"

class WorkStealingQueue {
    using DataType = FunctionWrapper;
public:
    WorkStealingQueue() = default;

    WorkStealingQueue(const WorkStealingQueue&) = delete;
    WorkStealingQueue& operator = (const WorkStealingQueue&) = delete;

    void push(DataType value);
    bool try_pop(DataType& result);
    bool try_steal(DataType& result);
    bool empty() const;

private:
    std::deque<DataType> m_workQueue;
    mutable std::mutex m_mutex;
};

#endif // WORKSTEALINGQUEUE_H
